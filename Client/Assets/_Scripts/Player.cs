﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public NetworkEntity networkEntity;
    public GameObject panelWinner;
    public SceneController sceneController;
    public GameObject panelLose;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Finish")
        {
            Debug.Log("hit");
            Network.Finish(networkEntity.id);
            panelWinner.gameObject.SetActive(true);
            panelLose.gameObject.SetActive(false);
            StartCoroutine(sceneController.BackToHome());
        }
        
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
