﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollow : MonoBehaviour
{
    public Transform playerTransform;
    private Vector3 _cameraoffset;

    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 0.05f;
    // Start is called before the first frame update
    void Start()
    {
        _cameraoffset = transform.position - playerTransform.position;
    }

    private void LateUpdate()
    {
        Vector3 newPos = playerTransform.position + _cameraoffset;
        transform.position = Vector3.Slerp(transform.position, newPos, SmoothFactor);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
