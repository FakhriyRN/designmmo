﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneController : MonoBehaviour
{
    public void StartMatch()
    {
        SceneManager.LoadScene("main", LoadSceneMode.Single);
    }
    public void Exit()
    {
        Application.Quit();
    }

    public IEnumerator BackToHome()
    {
        yield return new WaitForSecondsRealtime(5f);
        SceneManager.LoadScene("Main menu", LoadSceneMode.Single);
    }
}
